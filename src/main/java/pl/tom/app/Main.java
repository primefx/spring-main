package pl.tom.app;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.tom.app.xml.types.Category;
import pl.tom.app.xml.types.Dictionaries;
import pl.tom.app.xml.types.Dictionary;
import pl.tom.app.xml.types.User;

public class Main {

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws JAXBException {

		Class[] classes = new Class[] { Dictionaries.class, Dictionary.class, Category.class, User.class };

		JAXBContext context = JAXBContext.newInstance(classes);

		List<Category> subSubCategories = new LinkedList<>();
		subSubCategories.add(new Category(1, "subSubCategory 1", "", null));
		subSubCategories.add(new Category(2, "subSubCategory 2", "", null));
		subSubCategories.add(new Category(3, "subSubCategory 3", "", null));
		subSubCategories.add(new Category(4, "subSubCategory 4", "", null));

		List<Category> subCategories = new LinkedList<>();
		subCategories.add(new Category(1, "SubCategory 1", "", subSubCategories));
		subCategories.add(new Category(2, "SubCategory 2", "", null));
		subCategories.add(new Category(3, "SubCategory 3", "", null));
		subCategories.add(new Category(4, "SubCategory 4", "", null));

		List<Category> categories = new LinkedList<>();
		categories.add(new Category(1, "Category 1", "Ihha", null));
		categories.add(new Category(2, "Category 2", "<html></html>", null));
		categories.add(new Category(3, "Category 3", "doesnt work", null));
		categories.add(new Category(4, "Category 4<>", "", null));

		List<Dictionary> dictionaryList = new LinkedList<Dictionary>();
		dictionaryList.add(new Dictionary(1, "One", categories));
		dictionaryList.add(new Dictionary(2, "Two", null));

		Dictionaries dictionaries = new Dictionaries(dictionaryList);

		List<Long> list = new LinkedList<Long>();
		list.add(1L);
		list.add(2L);
		list.add(3L);
		list.add(4L);
		list.add(5L);
		list.add(10L);

		User user = new User();
		user.setBirthdate(new Date());
		user.setGender("M");
		user.setOrganizations(list);
		user.setRoles(list);
		user.setSites(list);
		user.setUserGroup(list);

		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		marshaller.marshal(user, System.out);
	}
}
