package pl.tom.app.xml;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CreatedOnAdapter extends XmlAdapter<String, Date> {

	SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy hh:mm");

	@Override
	public Date unmarshal(String v) throws Exception {
		if (v != null)
			return format.parse(v);
		return null;
	}

	@Override
	public String marshal(Date v) throws Exception {
		if (v != null)
			return format.format(v);
		return null;
	}

}
