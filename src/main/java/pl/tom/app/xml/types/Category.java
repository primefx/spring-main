package pl.tom.app.xml.types;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {

	@XmlElement(name = "sourceId")
	private long id;

	private String name;

	@XmlCDATA
	private String description = null;

	@XmlElementWrapper(name = "subcategories")
	@XmlElement(name = "category")
	private List<Category> categories = null;

	public Category() {
	}

	public Category(long id, String name, String description, List<Category> categories) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.categories = categories;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Category> getCategories() {
		return categories;
	}
}
