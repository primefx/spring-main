package pl.tom.app.xml.types;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dictionaries {

	@XmlElement(name = "dictionary")
	private List<Dictionary> dictionaries = null;

	public Dictionaries() {
	}

	public Dictionaries(List<Dictionary> dictionaries) {
		this.dictionaries = dictionaries;
	}

	public List<Dictionary> getDictionaries() {
		return dictionaries;
	}
}
