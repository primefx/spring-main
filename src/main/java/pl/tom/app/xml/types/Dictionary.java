package pl.tom.app.xml.types;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Dictionary {

	@XmlElement(name = "sourceId")
	private long id;

	@XmlElement
	private String name;

	@XmlElementWrapper(name = "categories")
	@XmlElement(name = "category")
	private List<Category> categories = null;

	public Dictionary() {
	}

	public Dictionary(long id, String name, List<Category> categories) {
		this.id = id;
		this.name = name;
		this.categories = categories;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
