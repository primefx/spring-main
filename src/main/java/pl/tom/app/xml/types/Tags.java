package pl.tom.app.xml.types;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tags")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tags {

	@XmlElement(name="tag")
	private List<Tag> tags = null;

	public Tags() {
	}

	public Tags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Tag> getTags() {
		return tags;
	}
}
