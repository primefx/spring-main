package pl.tom.app.xml.types;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import pl.tom.app.xml.BirthdateAdapter;
import pl.tom.app.xml.CreatedOnAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {

	@XmlElement(name = "sourceId")
	private long id;

	private String firstName;
	private String lastName;
	private String screenName;
	private String gender;
	private String email;

	@XmlJavaTypeAdapter(value = CreatedOnAdapter.class)
	private Date createdOn;

	@XmlJavaTypeAdapter(value = BirthdateAdapter.class)
	private Date birthdate;

	@XmlElementWrapper(name = "sites")
	@XmlElement(name = "site")
	private List<Long> sites;

	@XmlElementWrapper(name = "organizations")
	@XmlElement(name = "organization")
	private List<Long> organizations;

	@XmlElementWrapper(name = "roles")
	@XmlElement(name = "role")
	private List<Long> roles;

	@XmlElementWrapper(name = "userGroups")
	@XmlElement(name = "userGroup")
	private List<Long> userGroup;

	public User() {
	}

	public User(long id, String firstName, String lastName, String screenName, String gender, String email,
			Date createdOn, Date birthdate, List<Long> sites, List<Long> organizations, List<Long> roles,
			List<Long> userGroup) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.screenName = screenName;
		this.gender = gender;
		this.email = email;
		this.createdOn = createdOn;
		this.birthdate = birthdate;
		this.sites = sites;
		this.organizations = organizations;
		this.roles = roles;
		this.userGroup = userGroup;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public List<Long> getSites() {
		return sites;
	}

	public void setSites(List<Long> sites) {
		this.sites = sites;
	}

	public List<Long> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Long> organizations) {
		this.organizations = organizations;
	}

	public List<Long> getRoles() {
		return roles;
	}

	public void setRoles(List<Long> roles) {
		this.roles = roles;
	}

	public List<Long> getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(List<Long> userGroup) {
		this.userGroup = userGroup;
	}

}
